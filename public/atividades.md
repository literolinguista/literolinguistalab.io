## Atividades

> “Não sou contra que você entre na universidade, mas não deixe isso atrapalhar seus estudos.” Mark Twain.

### Projetinhos (mal)acabados...

- [Atividades de Libras](https://literolinguista.gitlab.io/libras "Atividades de Libras")
- [Narração do vestibular Letras Libras da UFPE, 2023.2](https://odysee.com/@jacksondejesus:1/narracaoVestibularLibras:0 "Narração Vestibular Letras Libras UFPE 2023.2"){target="_blank"}
- [Prevupe, pré-vestibular](https://literolinguista.gitlab.io/prevupe/ "Prevupe")
- [Haikais, publicação coletiva](atividades/escrita/haiku-paula-antunes-sergio-borges-org-22-04-2021.pdf "Haikais, publicação coletiva"){target="_blank"}
- [Redes livres: Palestra relâmpago na Python Brasil 2021](https://odysee.com/@jacksondejesus:1/palestraRelampagoJacksonJesusPyBR2021:2 "Redes livres: Palestra relâmpago na Python Brasil 2021"){target="_blank"}
- [UFPE no Meu Quintal, participação](https://www.youtube.com/watch?v=sjH2LXGlNQM "UFPE no Meu Quintal, participação"){target="_blank"}
- FLISol Recife 2019:
    - [Slide da apresentação](atividades/flisolRecife/flisolRecife2019Apresentacao.html "Slide da apresentação"){target="_blank"}
    - [Texto da palestra](https://meiotexto.wordpress.com/2019/05/01/sobre-alternativas-livres-para-redes-sociais-proprietarias "Texto da palestra"){target="_blank"}
- [As cores dos discursos em sala de aula](https://prezi.com/fcpm0vwsmm09/as-cores-dos-discursos-em-sala-de-aula/?present=1 "As cores dos discursos em sala de aula"){target="_blank"} (pretendo refazer no [Sozi...](https://sozi.baierouge.fr "Sozi"){target="_blank"})

[Voltar ao início...](index.html)
