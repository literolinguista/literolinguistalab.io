## Sobre o dia dos professores...

> "Feliz aquele que transfere o que sabe e aprende o que ensina." Cora Coralina.

No dia dos professores, só peço uma coisa aos meus estudantes: não nos ignorem na sala de aula (eu sei que vocês já moram mesmo no celular, mas...). Ser tratado como cantor de boteco dói, como qualquer outro tipo de rejeição. A sociedade já nos trata mal, o estado já nos considera inimigos, nosso único refúgio é a solidariedade de vocês. Se não temos isso, acabou. Porque quando preparamos aulas, só pensamos em vocês. Sabemos que tudo tem seu tempo e lugar. E nas aulas, sem vocês, não somos nada!

"Ah, professor, mas tem aula que é chata… ". Eu sei. Nem sempre é fácil converter um conteúdo abstrato para algo menos complicado de entender. Assim como nem sempre, vocês estão com cabeça para estudar. Mas vocês têm o direito à falta e nem precisam justificar, só se sentirem que o professor (da aula chata, mas que às vezes nem é chato) merece alguma justificativa. É menos pior faltar em corpo (e recuperar o assunto depois) do que permanecer em sala (s)em espírito.

"Ah, professor, mas o professor é ruim… ". Professor também é gente, igual a estudantes (na verdade, um bom professor geralmente é um bom estudante) e, da mesma forma, temos nossos dias ruins (mais do que gostaríamos). O que não justifica descontarmos nossos sofrimentos em vocês. Quem faz isso está errado (e nesses casos, talvez seja melhor mesmo morar no celular)! Mas tentem focar nos conteúdos. 

Imagine os dias de chuva, quando precisamos pegar a água e vamos na folia, segurando o balde no peito aberto, transbordando alegria! Já nos dias de granizo, encolhidos no casaco, jogamos uma bacia de alumínio amarrada na cordinha, para depois puxar as pedras com cuidado (autoproteção é essencial). Mas, seja lá qual for o tempo, o importante mesmo é pegar a água, independente de seu estado. Se ligaram na metáfora? Água, conhecimento...

"Ah, professor, mas o professor não sabe ensinar… ". Aqui neste texto, especificamente, estou me referindo aos professores de verdade, não aquelas pessoas que dão aulas apenas para receber um salário no final do mês. Me refiro à maioria de nós, que rimos e ficamos sérios com vocês, elaborando aulas antes das aulas (pensando em vocês), para depois corrigir as atividades (sempre pensando em vocês). Para aquele outro tipo de "professor" (sem alma de professor), a mesma dica anterior, foco na água. Depois, e antes de beber, vocês passam ela no filtro (o importante mesmo é pegar a água)...

Porque sempre haverá dificuldades, sempre, mas...

> "Professores e estudantes são faces de uma mesma moeda."

Professores de verdade são aqueles que trabalham, mesmo sabendo que essas horas fora da sala de aula nunca serão pagas em dinheiro (mas com a alegria de vocês), e mesmo assim (e por isso) nos dedicamos com esforço e carinho, sempre com a imagem das carinhas de vocês coladas em nossos pesamentos. Porque dar aulas é a parte menos difícil desse processo. Na verdade, na verdade, mesmo, dar aula é a melhor parte, é a materialização de nossa alegria, porque compartilhada com vocês, estudantes. 

E uma boa aula, uma aula boa, é aquela em que vocês aprendem, como uma boa conversa, onde todos participam e, no final, todo mundo sai feliz, porque, pouco ou muito, cada um contribuiu com um pedacinho de si, fazendo parte da roda. Lembrem-se disso: os melhores professores, na verdade, só são bons porque são reflexos das melhores turmas e o melhor de cada turma são vocês, estudantes. Não existe professor bom para um único estudante (só na cabecinha dele). Sem vocês, não somos nada!

Esse é meu único pedido. Se, ainda assim, quiserem me dar presente, gosto de livros, doces, cadernos (principalmente artesanais), ingressos de cinema e de fazer cursos caros da internet (mentira, eles não são caros, eu que sou liso)! E se gostou deste texto, talvez também goste das sugestões abaixo (não obrigatórias, claro):

- Dizem que sou um bom professor, talvez este texto confirme (ou não) essa impressão: [Sobre a pedagogia do encanto...](https://literolinguista.gitlab.io/textos/encanto.html "Sobre a pedagogia do encanto..."){target="_blank"}
- Precisa de um resuminho de inglês? [Tá na mão!](https://literolinguista.gitlab.io/prevupe/gramatica/index.html "Gramática de inglês: resumo..."){target="_blank"}
- Sabe o bicho-papão? Então, dizem que ele existe: [Sobre o mito da doutrinação escolar...](https://literolinguista.gitlab.io/textos/mito-doutrincao.html "Sobre o mito da doutrinação escolar..."){target="_blank"}
- Quer aprender a estudar melhor? [Fichamento: o que é e como se faz...](https://literolinguista.gitlab.io/textos/fichamento.html "Fichamento: o que é e como se faz..."){target="_blank"}
- Quer entender um pouco de informática? [Informática: conceitos básicos...](https://literolinguista.gitlab.io/textos/informatica-conceitos-basicos.html "Informática: conceitos básicos..."){target="_blank"}
- Não sei se sabem, mas sou um hacker (do bem, mas um escritor do mal): [Sobre hacker...](https://literolinguista.gitlab.io/textos/hacker.html "Sobre hacker..."){target="_blank"}
- E por fim, [imagens de um professor em seu habitat natural...](https://www.instagram.com/p/CybFfeSOqr8/?img_index=2 "Instagram"){target="_blank"}

[Voltar para a página Textos...](../textos.html)
