## Sobre 'Café e romance' (livro da Denise Gals)...

> "Que ninguém se engane, só se consegue a simplicidade através de muito trabalho." Clarice Lispector

# Atualização
> "Até o fim do mês de maio, todos os recebimentos da compra do e-book Café & Romance serão doados para uma instituição de SOS a Mulheres e Crianças no Rio Grande do Sul..." [Texto completo na postagem do Instagram](https://www.instagram.com/p/C67BpuaPEHe/){target="_blank"}

"Café e romance" é um achado da escritora Denise Gals, um livro sobre uma mulher em busca de seu antigo sonho de escrever romances e abrir uma livraria café; um mote bem simples. 

Mas a autora é muito habilidosa no desenvolvimento da estória porque ela vai relevando aos poucos as sutilezas de cada personagem (que se materializam em nossa frente) e vai costurando, de um jeitinho muito engenhoso, as nuances de cada acontecimento, sem pressa, criando assim uma estória aparentemente comum, sobre o cotidiano, porém, profundamente envolvente. Por isso, este texto de admiração e indicação de leitura.

Assim com já disse a Clarice Lispector, Raimundo Carrero, um escritor daqui de Recife, no seu livro "A preparação do escritor", também nos avisa que a gente que escreve precisa sofisticar para simplificar. No sentido em que 

> "o escritor deve realizar no texto literário o máximo de sofisticação (...) para alcançar o máximo de simplicidade. (...) Escrever com muito esforço para ser lido com a facilidade de quem bebe água. A nossa tarefa é seduzir." 

Eu não sei se a Denise escreveu com muito esforço, mas realmente a gente lê o livro dela com a mesma facilidade de quem bebe água, e com sede. É incrível o efeito sedutor de sua escrita sobre nós! E obviamente, eu, que sou dublê de escritor, ainda vou fazer uma releitura técnica para tentar aprender como ela faz para escrever assim, de forma tão envolvente e bonita!

Ainda não terminei porque estou no dilema de sempre, quando encontro uma obra que gosto muito, que é a alegria da fruição e a tristeza da perspectiva do fim da leitura. Porque o livro também é dividido em capítulos bem curtinhos (inclusive, a maneira como ela apresenta os títulos...), mas que dão aquela sensação de parcial completa. 

Após ler cada capítulo, a gente fica com essa impressão de que sabe de tudo que era preciso saber sobre aquele recorte. É como se ela comprimisse o tempo em poucas páginas e, enquanto a gente vai lendo, ele vai se expandindo em nossa mente. 

O que também me dá aquele sentimento bom de pausa reflexiva (a fruição da leitura que tanto falo), de que preciso parar, sentir essa parte da estória um pouco, para ler mais depois. Eu adoro isso, acho uma habilidade incrível quem consegue esse efeito, e me pareceu, não tenho certeza, algo intencional e minuciosamente calculado pela autora. 

De qualquer forma, não tem como a gente não se identificar com Diana (a protagonista, me identifiquei muito com ela), nem como a gente não ter certeza de que a amiga dela é uma sociopata e o namorado uma pessoa ótima, principalmente dormindo (ironia para dizer que eles são, às vezes, insuportáveis!)...

Mas isso já são minhas opiniões sobre os personagens. O que sei é que o livro é excelente, pretendo levá-lo nas minhas aulas de linguagens e por isso estou aqui recomendando! Ele está disponível na Amazon [(neste link)](https://encurtador.com.br/osW46){target="_blank"} com preço acessível, lembrando que preço não é o mesmo que valor. 

Vocês lembram que eu sou um leitor desarmado, gosto de ler boas estórias e bem contadas. Para mim, "Café e romance" cumpre esses dois requisitos e muito mais, por isso o considero, literariamente, um dos melhores livros que já li (mesmo sem terminar). Espero que gostem, porque a Denise deu um presente para a gente e, por isso, já sou grato!

Boa leitura.

[Voltar para a página Textos...](../textos.html)