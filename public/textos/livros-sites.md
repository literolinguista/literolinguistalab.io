## Sites de livros

> "A leitura do mundo precede a leitura da palavra." Paulo Freire.

### Sites para baixar livros grátis

Uma pessoa amiga me perguntou onde consigo livros na Internet, achei bom reunir alguns links (legais, outros nem tanto, se é que me entendem, rs) nesta página.

- [Amazon](https://www.amazon.com.br/s?bbn=5308307011&rh=n%3A5308307011%2Cp_36%3A5560478011&dc&_encoding=UTF8&fst=as%3Aoff&linkCode=sl2&linkId=1ae6c96b7a05ccad94ed6aa8adaf15e7&qid=1585913459&rnid=5560477011&tag=mi02a0-20&ref=lp_5308307011_nr_p_36_0 "Amazon"){target="_blank"}
- [Anna’s Archive](https://annas-archive.org "Anna’s Archive"){target="_blank"} (Sugestão do [sol2070](https://sol2070.in/){target="_blank"}, valeu pela dica!)
- [Baixe livros](https://www.baixelivros.com.br "Baixe livros"){target="_blank"}
- [Biblioteca Brasiliana Guita José Mindim (BBM)](https://www.bbm.usp.br/pt-br "Biblioteca Brasiliana Guita José Mindim (BBM)"){target="_blank"}
- [Biblioteca de São Paulo Digital (BSP)](https://biblion.odilo.us "Biblioteca de São Paulo Digital (BSP)"){target="_blank"}
- [Biblioteca Nacional Digital Brasil](https://bndigital.bn.gov.br "Biblioteca Nacional Digital Brasil"){target="_blank"}
- [Cultura Acadêmica](https://www.culturaacademica.com.br "Cultura Acadêmica"){target="_blank"}
- [dLivros](https://dlivros.com "dLivros"){target="_blank"}
- [elivros](https://elivros.love "elivros"){target="_blank"}
- [Google Livros](https://books.google.com.br "Google Livros"){target="_blank"}
- [Lê livros](https://www.lelivros.app "Lê livros"){target="_blank"}
- [Lê livros grátis](https://lelivrosgratis.com "Lê livros grátis"){target="_blank"}
- [Librivox](https://librivox.org "Librivox"){target="_blank"}
- [Livraria do Senado](https://livraria.senado.leg.br "Livraria do Senado"){target="_blank"}
- [Livros Grátis](http://livrosgratis.com.br "Livros Grátis"){target="_blank"}
- [Machado de Assis](https://machado.mec.gov.br "Machado de Assis"){target="_blank"}
- [OpenLibrary](https://openlibrary.org "OpenLibrary"){target="_blank"}
- [Portal Domínio Público](http://www.dominiopublico.gov.br "Portal Domínio Público"){target="_blank"}
- [Projeto Gutenberg](https://www.gutenberg.org "Projeto Gutenberg"){target="_blank"}
- [sci-hub](https://sci-hub.hkvisa.net "sci-hub"){target="_blank"}
- [sci-hub português](https://scihub.vip/?lang=pt "sci-hub português"){target="_blank"}
- [Telegram, canais](https://telegram-channel.net/pt/channel/category/books "Telegram, canais"){target="_blank"}
- [Telegram, grupos](https://mundointerpessoal.com/2020/04/grupo-telegram-livros-pdf-literarios.html "Telegram, grupos"){target="_blank"}

[Voltar para a página Textos...](../textos.html)