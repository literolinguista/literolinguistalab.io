## Sobre Contos revistos (autopublicação na Amazon)...

> "A vida é a arte do encontro, embora haja tanto desencontro pela vida." Vinícius de Moraes.

Publiquei na Amazon (enfim!) uma pequena coleção de contos (alguns curtinhos), ["Contos revistos (retalhos de escrita)..."](https://www.amazon.com.br/dp/B0DJWWKZXP "Contos revistos (retalhos de escrita)..."){target="_blank"}. Chamei assim porque são textos revisados que já foram publicados antes, o primeiro, numa antologia chamada "Revelações Literárias: contos, crônicas e poemas" e o restante, no meu perfil do Recanto das Letras. 

A ideia é me apresentar como autor, para futuras publicações inéditas ("autobiscoitagem" é melhor que autossabotagem, rs). Por isso, coloquei um preço simbólico que, como bem sabemos, difere muito de valor subjetivo, inerente e relativo a qualquer produção artística.

Para esta primeira publicação (autopublicação, na verdade), pedi ajuda de revisão para algumas pessoas amigas, mas elas não puderam. Mas uma estudante querida e também desenhista aceitou imaginar dois dos personagens do primeiro conto. De qualquer forma, agradeço a boa intenção de todos.

No passado, sempre deixava de finalizar as coisas porque nunca estava satisfeito com os resultados, perdi muitas oportunidades assim. Por diversas vezes, também, já tinha escutado que é melhor entregar algo bom no prazo do que nunca entregar o "perfeito", mas sempre ficava preso nessa ilusão de perfeição e nunca conseguia finalizar nada... 

Até que um dia, ouvi a mesma coisa, mas de um jeito diferente (com o coração), e compreendi finalmente o que precisava ser feito: apenas começar. Depois que a gente começa, os ajustes podem ser feitos, mas é preciso ter esse começo; algo para se revisar depois. Foi assim que nasceu este site e todas as outras coisas bonitas que consegui fazer e das quais me orgulho!

Por fim, pessoalmente, não gosto da ideia de monopólio (Amazon), mas sou apenas mais um passarinho migrando atrás do calor da vida no frio do mundo. Então, se você não quiser/puder acessar por lá, pode baixar esta [versão](contos-revistos.pdf){target="_blank"} (levemente diferente da disponibilizada lá, por questões de contrato). Espero que gostem!

Para saber das coisas que faço, tenho uma página de [links](https://literolinguista.github.io "literolinguista"){target="_blank"}.

[Voltar para a página Textos...](../textos.html)
