# Sobre o pensamento computacional e o poético…

> “A pintura é poesia muda; a poesia, pintura cega.” Leonardo da Vinci.

Por que quem escreve códigos (pessoas programadoras) também deveriam escrever poemas?

**Resposta curta.**

Porque, em certa medida e em relação à criatividade, os primeiros nos limitam, enquanto os segundos nos estimulam.

**Resposta longa.**

Códigos são conjuntos reduzidos de instruções que damos aos processadores. Poemas são materializações atemporais e n-dimensionais da poesia no texto escrito ou oral. Lembrando, poesia é aquele sussurro rítmico que nos sopra o espírito e se manifesta através das artes (plásticas, cênicas, literárias, musicais…). Os primeiros são construções humanas, a segunda (dizem), revelação divina.

Poemas, então, são bloquinhos leves de textos escritos (e declamados), semanticamente saturados de imagens poéticas voláteis, como fótons borboletas voando despreocupadas próximos a discos de acreção das singularidades cósmicas; por isso, no poema, também são possíveis divisões por zeros.

> Assim como a gravidade é o efeito da distorção de corpos massivos no tecido espaço-tempo, o discurso poético é o efeito de sentidos nos espíritos dotados de linguagem.

Um poema relido jamais replica os mesmos efeitos da primeira leitura, nunca! O poema é o mesmo, enquanto materialidade textual, a leitura, enquanto processo de construção de sentidos, não. As variáveis tempo e subjetividades se transpassam em sentimentos diversos, como partículas colidindo em aceleradores e decaindo em outras sensações, novas e fundamentais. 

E da mesma forma que as partículas ganham massa nas interações com o campo de Higgs, cada (re)leitura de um mesmo poema fornece imagens (e altera o estado de consciência) na realidade de quem o lê (se assim o permitir, claro).

Em contraste, o código de computador, apesar das refatorações (diversas possibilidades de arranjos de suas variáveis), ainda assim, sempre dará um mesmo e esperado resultado. O determinismo, ainda que aleatório, faz parte de sua natureza, enquanto objeto virtual e rascunho de simulacro da realidade que tenta representar.

O processador binário, apesar de incrivelmente rápido, não possui aquele “sussurro inspirador”, nem se expande como uma mente antes de inalar (e às vezes, quase se afogar) no momento da criação (incluindo a literária). O processador trava em seus processamentos e a IA (inteligência artificial) até alucina. Mas apenas um cérebro orgânico se contrai e se dilata a cada sentimento, o pulso do amor-razão (aquela ilusão vinda do coração).

No sentido oposto, até as alucinações da IA orgânica (indivíduo atormentado) servem de matéria-prima para a escrita, literária e poética. Podemos escrever poemas com códigos (e vice-versa) e podemos ensinar a IA gerativa a copiar nosso jeito de escrever códigos com poemas (e vice-versa), mas apenas o jeito… porque programar também pode ser uma atividade artística, mas a atividade artística… entendeu né?

Falando em servir, a arte não serve para nada, nem ninguém, nesse sentido capitalista de serviço. Relembrando Ferreira Gullar, “A arte existe porque a vida não basta”, fazemos arte (tocados pela poesia) para viver outras vidas. É quase impossível aprender todas as linguagens de programação atuais, mas muito fácil experienciar todas as estórias d'As mil e uma noites desde o passado…

A linguagem de máquina é isso, uma redução simplória do pensamento humano. Talvez daí a dificuldade de muitos em programar. Como encaixar algo como criatividade em casquinhas de amendoins do tamanho de bits? Se for seu caso, não se sinta menos inteligente por isso; inteligência é qualidade (habilidade que se desenvolve), não quantidade. 

Ser bom em qualquer atividade tem a ver com esse esforço consciente de desenvolvimento. O *“saber de cor”,* do latim/francês, *“savoir par coeur”,* é uma expressão do tempo em que se acreditava que este órgão comandava a razão, e com sentido de saber automático. Talvez por isso, muitos se sintam perdidos nas “rotas antigas” das novas linguagens e frameworks. Isso mesmo, por enquanto, uma linguagem nova é só um caminho diferente para se chegar num mesmo e já conhecido lugar.

Por isso é importante lembrar, a Ada Lovelace nos abstraiu problemas reais em códigos (virtualização) para a gente viver a vida (realização), não para continuarmos apertadores de botões. A ideia é a liberdade do pensamento (criatividade), não a prisão. Atualmente, a alegria da codificação vem mais da percepção da falta do ponto e vírgula após horas ou dias de angústia, do que da real criação do desejado objeto virtual; uma falsa alegria, portanto.

Já a alegria da escrita, literária ou poética (ou produção artística computacional), por sua vez, vem da consciência estética e fruição da leitura. O texto final nunca existe, porque é um *continuum* no espaço-tempo, uma grande conversa de gerações de leitores passadas, presentes (ausentes) e futuras. Daí a necessidade de esticar a mente através da escrita (arte), principalmente a poética.

Porque o pensamento computacional é apenas seguir caminhos rígidos de um mundo deserto (capital) vazio e árido. Pensamento poético (artístico), entretanto, é escrever as próprias trilhas na floresta da vida, buscando aproveitar o que de melhor for possível, como no poema “Existência”, de Valdelice Soares Pinheiro:

```
Desenho a mão
que faço
em meu destino,
confuso o pé
que traço
em Meu caminho.

Eu sou
na estranha vertigem
dessa estrada,
meu ponto de partida
e de chegada.
```

[Voltar para a página Textos...](../textos.html)
