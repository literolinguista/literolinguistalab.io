## Sobre “capaternismo” ou “patercitismo”...

> Dedico as pessoas amigas surdas.

A gente vivi numa sociedade cheia de preconceitos e por isso, é difícil não replicarmos alguns. É como andar numa rua alagada em dia de chuva, um pulinho aqui, outro acolá, e mesmo assim vamos nos sujar, e todo mundo vai ver a sujeira.

Recentemente, devido às “redes sociais" [Ligação externa 1 (neste outro texto explico porque uso esse termo entre parênteses)
](https://meiotexto.wordpress.com/2019/02/26/sobre-software-livre/ "Sobre software livre..."){target="_blank"}, tanto nossa intolerância quanto o combate a ela ficou mais evidente. Aos poucos, estamos nos tocando sobre nossos grandes preconceitos, como racismo, machismo, misoginia, homofobia, transfobia, preconceito religioso e linguístico (na verdade, preconceito de classe disfarçado de linguístico…), dentre outros. Mas, tem um que ainda parece passar despercebido: o capacitismo (com o paternalismo).

Em resumo, capacitismo é o preconceito contra pessoas com deficiência. E talvez, por está relacionado a esse termo não muito preciso (deficiência), o capacitismo ainda não seja percebido como uma grande mancha de lama em nossa roupa, naquele dia de chuva.

O termo deficiência me parece impreciso, porque está relacionado a ideia de ausência e/ou falta, só que esta ausência e/ou falta é imposta de forma acrítica, a partir de um padrão de normatividade já dado como natural, e desde sempre. Segundo esta lógica, uma pessoa com deficiência é aquela a qual falta algo que a maioria tem.

Mas, e se em todos os lugares houvesse rampas adaptadas para pessoas cadeirantes, placas com textos adaptados ou em braile para pessoas cegas, ou com baixa visão, ambientes livres de perturbações sensoriais para pessoas autistas e neurodiversas e, se tivéssemos acessibilidade em Libras para pessoas surdas? Já imaginou um mundo assim?

Quando refletimos sobre estas questões, chegamos a conclusão que a ideia de deficiência é vista apenas pela perspectiva patológica, de ausência e/ou falta, mas nunca pela perspectiva inclusiva, de responsabilidade social coletiva de prover suporte para as necessidades específicas dessas minorias (algumas delas, grupos minorizados).

Por isso prefiro o termo necessidade específica, porque cada um dos grupos acima citados tem necessidades específicas que poderiam ser supridas se houvesse o mínimo de disposição de nossa parte, enquanto sociedade. É muito fácil chamar o outro de deficiente quando não se precisa de nenhum tipo de suporte específico. Acessibilidade dá trabalho, mas essas pessoas também não pagam impostos?

**Atualização 26.10.24**

Interagindo com [essa postagem da Geisa Farini, sobre expressões capacitistas](https://www.instagram.com/p/DBmhYsbJHg6){target="_blank"}, fui alertado que o termo “pessoas com necessidades específicas” também é impreciso porque é mais abrangente e contempla, por exemplo, mulheres grávidas, pessoas obesas, idosas, com mobilidade reduzida momentaneamente (por cirurgias), etc. Sempre bom conversar com quem experiencia essas vivências!

Dito isso, um dos preconceitos que mais me incomoda, principalmente no Instagram, são vídeos de pessoas, majoritariamente ouvintes, vendendo curso de Libras sempre usando variações da frase “como ajudar o surdo…”, como se a pessoa surda fosse sempre a necessitada.

As pessoas surdas estão aí, vivendo suas vidas desde sempre, trabalhando, pagando seus impostos, cumprindo seus deveres sem nem sempre terem seus direitos garantidos e lidando com nossa ignorância em relação a eles desde sempre. Como todo grupo que sofre preconceitos, a pessoa surda também desenvolve resiliência.

Essa insistência em “paternalizar” a necessidade específica da pessoa surda, a qual é uma bem específica, pois linguística, é o trocadilho que fiz no título do texto, “capaternismo” ou “patercitismo”, que me parece ser a intersecção do capacitismo com o paternalismo. Caberia um “capacinismo” também…

Interseccionalidade é um conceito (que vale a pena ser estudado) criado pela Kimberlé Crenshaw e que, em resumo, tem a ver com “formas de capturar as consequências da interação entre duas ou mais formas de subordinação…”. Aqui no caso, a relação entre capacitismo e paternalismo e a malefícios decorrentes, como os diversos preconceitos imputados à pessoa surda.

Por fim, a pessoa surda não é “alguém” com uma ausência e/ou falta incapacitante, ela possui sua própria língua; que por um acaso, se comemora hoje, dia 24 de abril. Você já ouviu alguém dizer: “vou aprender inglês para ajudar um americano…”? Se isso soa estranho, por que não nos incomodamos quando a língua em questão é a Libras?

E como disse antes, e nunca me repito, sei que é difícil não replicarmos preconceitos quando estamos imersos numa sociedade repleta deles, mas isso não nos isenta de nossa responsabilidade pessoal, se quisermos compartilhar as oportunidades com equidade social (igualdade com justiça).

Ah, esta não é a primeira vez que escrevo sobre Libras [Ligação externa 2 (texto antigo sobre Libras)](https://meiotexto.wordpress.com/2017/09/08/sobre-libras-ou-coisas-importantes-que-fazemos-questao-de-nao-notar/ "Sobre Libras..."){target="_blank"}

[Voltar para a página Textos...](../textos.html)
