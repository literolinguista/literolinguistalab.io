## Carta para a pessoa amiga...

> "A amizade é um amor que nunca morre." Mario Quintana.

Querida pessoa amiga, você é especial. Não falo isso da boca para fora. Eu vi sua tristeza por trás de sua alegria. E também vi sua luz por trás de sua tristeza. Quem também já teve o espírito fraturado, consegue enxergar mais fácil a luz das pessoas especiais. Não quero te confundir, apenas justificar o que parece óbvio, o porquê de você ser uma pessoa especial. Porque alegrias e tristezas vão e vem, mas a luz (essa mesma luz que sai por trás de seu coração partido e ainda assim a todos cega), essa luz permanecerá, porque ela é perene e vem do mesmo lugar da origem da vida.

Talvez por isso o mundo tente sempre te decepcionar, com tristezas que parecem sem fim. Porque ainda assim sua luz encandeia! Por isso, querida pessoa amiga, me sinto na obrigação de te relembrar que essa sua luz é vida. E vida é diferente de mundo. Vida é essa coisa inexplicável, igual à água fresca quando a gente está morrendo de sede. Às vezes a água é doce, às vezes nem tanto, mas sempre, sempre satisfaz... é assim a vida.

O mundo, o mundo é só o recipiente que construímos para beber a vida. Não confunda os dois, querida pessoa amiga. O mundo sempre nos bate forte. O mundo dói, ele quer sempre a gente no chão. Se dependesse do mundo, rastejaríamos de joelhos. Mas a vida, a vida é aquela água fresca de sempre. A vida é quem nos diz: o que importa não é cair, mas se levantar, sempre.

Por isso insisto, mais uma vez, querida pessoa amiga. O mundo é só o recipiente o qual bebemos e às vezes celebramos a vida! Às vezes ele é um copo limpo e bonito, às vezes nem tanto. Mas uma coisa é certa, a água (a vida), essa é sempre excelente e sempre, sempre satisfaz.

De seu amigo de sempre, o tempo.

[Voltar para a página Textos...](../textos.html)