## Séries que me impressionaram!

> “Gostar é provavelmente a melhor maneira de ter, ter deve ser a pior maneira de gostar.” José Saramago.


Quando sobra um tempo, assisto umas séries [(...)](# "pelo aplicativo Stremio porque sou pobre de dinheiro, rs"). Estas são algumas, mais ou menos recentes, que me marcaram.

- [Arcane](https://www.adorocinema.com/series/serie-29127 "Arcane"){target="_blank"}
- [Fundação](https://www.adorocinema.com/series/serie-18438 "Fundação"){target="_blank"}
- [Halo](https://www.adorocinema.com/series/serie-12209 "Halo"){target="_blank"}
- [Love, death & robots](https://www.adorocinema.com/series/serie-24635 "Love, death & robots"){target="_blank"}
- [O senhor dos Anéis: Os anéis do poder](https://www.adorocinema.com/series/serie-22940 "O senhor dos Anéis: Os anéis do poder"){target="_blank"}. Mudei de ideia depois de ver [esta crítica bem fundamentada da Liana Montenegro, do canal Cine Jam](https://www.youtube.com/watch?v=cumXP4VLrAI "OS ANÉIS DE PODER: A ANTÍTESE DE 'O SENHOR DOS ANÉIS'"){target="_blank"}
- [Periféricos](https://www.adorocinema.com/series/serie-23551 "Periféricos"){target="_blank"}
- [Raised by wolves](https://www.adorocinema.com/series/serie-24255 "Raised by wolves"){target="_blank"}
- [The boys](https://www.adorocinema.com/series/serie-22668 "The boys"){target="_blank"}
- [The nevers](https://www.adorocinema.com/series/serie-23845 "The nevers"){target="_blank"}
- [The witcher](https://www.adorocinema.com/series/serie-22146 "The witcher"){target="_blank"}
- [Wandinha](https://www.adorocinema.com/series/serie-28487 "Wandinha"){target="_blank"}

A maioria delas, indicações do canal [Futurices](https://www.youtube.com/@Futurices "Futurices"){target="_blank"}, da Bela Eichler (pense numas resenhas excelentes!). Lembrando que omiti séries do universo de Star Wars, Star Trek e Marvel, seria uma lista grande... além de outras antigas, como Breaking Bad, Dexter, Dr. House, Game of Thrones, Supernatural, True Blood...

[Voltar para a página Textos...](../textos.html)