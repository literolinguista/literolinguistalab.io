## Sobre a pedagogia do encanto...

> "Aprender é, essencialmente, deixar o adulto de lado e trazer a criança curiosa de volta." Fábio Akita.

Meus estudantes e amigos dizem que eu tenho o "dom" de ensinar. Se você já me leu um pouco, sabe que não acredito neste mito, porque é sobre a ilusão de uma habilidade sobrenatural. Não dá para se iludir no chão da escola, não temos tempo nem recursos para isso; mas dá para projetar futuros possíveis juntos, sonhar, os professores e estudantes; isso a gente pode e até deve tentar, sempre que possível.

Eu não sei explicar o porquê do encantamento deles em relação às minhas aulas (e esse texto é uma tentativa pessoal de resposta). Não me sinto de jeito nenhum especial, nunca tive autoestima nem ambição (que é diferente de ganância, a qual é a ambição desmedida e também nunca tive), nunca me considerei inteligente (sei que tenho um pouco de erudição, que tem a ver com quantidade de leituras, que inclusive me fizeram compreender que inteligência mesmo, tem a ver com qualidade e em como a gente resolve os problemas do dia a dia e, sinceramente, minha condição de pessoa adulta é uma prova de que não sou inteligente) e para não me alongar, sou introvertido e não gosto de conhecer nem de conversar com pessoas estranhas...

Tudo isso é totalmente contraditório para alguém que se predispõe à atividade docente. Mas, não sei explicar, a sala de aula é diferente. Na minha cabeça, apenas sigo alguns ritos dos quais já participei enquanto estudante, mas tomando o cuidado de não replicar as partes que me causaram sofrimento...

Por exemplo, nunca grito, e acho desnecessário explicar o porquê. Nas turmas de sexto ano, é comum alguns professores gritarem para (pasmem) pedir silêncio. Sempre que alguns estudantes me pediam para "por ordem" na sala desse jeito (replicando este ritual), sempre também os lembrava o quão contraditório é pedir silêncio gritando... A sala de aula é cheia de rituais institucionalizados, eu gosto de rituais, mas alguns deveriam ser abolidos...

Uma analogia que imaginei para tentar explicar (minha contradição), seria dizer que ensinar é como passear numa trilha (aula). A pessoa professora tem que saber os caminhos (competência técnica) e às vezes pegar os atalhos só em último caso, até o destino (objetivos da aula); ciente de que os estudantes talvez conheçam (ou não) algumas paisagens que verão durante o passeio. O detalhe mais importante (e curioso) dessa aventura é que a pessoa professora deve andar de costas para o lugar onde eles estão indo (ou seja, dar passos de Curupira), mas de frente para seus estudantes (exercício de empatia, uma habilidade que pode ser desenvolvida).

A pessoa professora não precisa ver o caminho, porque apesar de toda trilha (processo) ser uma nova e diferente (um dia pode fazer sol, outro chover…), ela já deve ter passado por lá de várias maneiras e ocasiões diferentes (preparação das aulas), a ponto de saber o caminho de cor (*coeur*, do francês, coração) e salteado. Ela precisa ir assim (de costas para o destino e de frente para os estudantes) porque só desse jeito ela pode perceber nas feições de cada um deles se o passeio está agradando ou não, se a trilha está muito difícil ou não e, dessa forma, fazer os (re)ajustes necessários para que a jornada seja proveitosa para a maioria.

A gente sempre vai conseguir chegar ao destino com todos e, ao mesmo tempo? Com certeza não. Vamos conseguir agradar a todos ao mesmo tempo? Nunca. Mas sempre vamos ter condições de enxergar aqueles que não aproveitaram tão bem a trilha, para em outro momento, se possível, tentar outras oportunidades...

No filme Matrix (releitura moderna do Mito da caverna de Platão), Morpheus diz que "Há uma diferença entre conhecer o caminho e percorrer o caminho." que é uma maneira diferente de expressar o que Heráclito também já dizia sobre “Ninguém se banha duas vezes no mesmo rio.” Nem o rio, nem o caminho, nem as pessoas, nem o tempo que cada uma vai passar por ele será o mesmo. Ou, finalizando o pacote de citações, como bem expressa Thiago de Mello: "Não tenho um caminho novo. O que eu tenho de novo é um jeito de caminhar." Ensinar e aprender são faces de uma mesma moeda e o valor dela reside (contraditoriamente) na exploração (viagem) da descoberta; e na sua circulação.

A importância da pessoa professora nesse processo é que ela ajuda na criação e manutenção de um contexto favorável ao aprendizado, ela aponta os caminhos para os estudantes construírem suas próprias experiências ao percorrê-los por elas mesmas; e voltarem felizes com o que lá descobriram. Ou, como melhor dizia nosso amigo Paulinho:

> "Ensinar não é transferir conhecimento, mas criar as possibilidades para a sua própria produção ou a sua construção." Paulo Freire.

Pensando sobre tudo isso, talvez a minha habilidade em repassar as coisas que aprendo (e gosto) tenha a ver com o fato de eu ainda guardar, não apenas na memória, mas no jeito de ser, a criança curiosa (no estágio das operações formais, segundo a teoria Construtivista de Piaget) que nasceu lá no final da década de 70 e convive comigo (adulto) até hoje. É essa mesma criança que reconhece a criança (às vezes escondida) dos meus estudantes na sala de aula e com elas compartilha a alegria da exploração do conhecimento; porque, no final, a pedagogia do encanto é, na realidade, alegria compartilhada!

![Dia em que aprendi (mais ou menos, rs) a jogar UNO...](melhorTurma.jpg "Imagem de estudantes e professor em sala de aula.")

[Voltar para a página Textos...](../textos.html)