## Fichamento: o que é e como se faz...

> Para meu amigo Gil (em memória).

Segundo a maioria dos dicionários da língua portuguesa, fichamento é simplesmente o ato ou efeito de fichar, ou seja, anotar, registrar e/ou reescrever informações selecionadas, as consideradas mais importantes, em fichas, para catalogação, classificação ou organização desses trechos selecionados para usos posteriores.

> “Criado no século XVII pelo Abade Rozier, da Academia Francesa de Ciências, o sistema de fichas é atualmente utilizado nas mais diversas instituições, para serviços administrativos, e nas bibliotecas, onde, para consulta do público, existem fichas de autores, de títulos, de séries e de assuntos, todas em ordem alfabética.” Trecho extraído da quinta edição do livro Fundamentos da metodologia científica, de Marina de Andrade Marconi e Eva Maria Lakatos; na página 50.

Nos dias em que vivemos, era da (des)informação, o fichamento (a adaptação aqui apresentada) é uma excelente ferramenta de estudo, tanto porque proporciona ao estudante a leitura do texto quanto também, a sua reflexão sobre a seleção das ideias contidas nele; além de sua posterior escrita no papel, isto é, no registro organizado da informação.

Então, para fichar é preciso primeiramente selecionar a unidade textual (um trecho, um capítulo, um livro) ler essa unidade selecionada (que pode ser verbal ou não verbal), durante esta leitura já é possível fazer a seleção das ideias consideradas importantes (mas esta seleção também pode ser feita numa segunda leitura) e fazer finalmente a transcrição para as fichas, ou seja, a (re)escrita das partes selecionadas no texto; na nossa adaptação podemos usar um caderno comum e/ou um arquivo de texto e suprimimos o texto omitido com o sinal de reticências entre parênteses (...), como veremos nos exemplos adiante.

**Aviso importante**: fichamento não é resumo! A ideia é usá-lo com base para resumos, resenhas, artigos, etc., e não mais voltar ao texto original.

#### Mas o que selecionar? Como selecionar?

Geralmente, como o fichamento tem por objetivo organizar informações consideradas relevantes ou importantes de um determinado texto, a seleção das ideias deve ser feita de modo que assinale o que há de principal no texto. E para saber ou descobrir o que há de mais importante no texto (parágrafo), uma sugestão é fazer as seguintes perguntas: o que o autor quer dizer, porque ele quer dizer, o que diz, como ele diz, quando, onde, etc. Quanto mais próximo um fichamento estiver das respostas para essas questões, mais significativo ele será.

#### Esquema do fichamento (sugestão de adaptação)

Leitura (o texto pode ser verbal ou não verbal, imagem, filme...);
Seleção (principais ideias a partir das questões possíveis...);
(re)Escrita (registro organizado das ideias para posterior consulta).
Existem tipos de fichamento? Quais são?
Existem várias maneiras de fichar, vários tipos de fichamento, dentre eles o fichamento bibliográfico, o fichamento de conteúdo e o fichamento de citação. Cada um com um objetivo específico a partir do texto e baseado num modelo, mas nada impede que se usem os vários tipos num mesmo fichamento. E cada pessoa vai fazer um fichamento conforme o seu jeito, a partir das suas leituras, com as suas próprias perguntas. Portanto, apesar de ser um método apoiado em modelos, o fichamento é uma ferramenta de estudos individuais.

#### Descrição dos tipos de fichamento?

O fichamento bibliográfico consiste na descrição resumida dos tópicos abordados numa obra inteira ou parte dela. O fichamento de conteúdo é aquele que descreve, também resumidamente, o conteúdo, ou seja, é uma síntese das principais ideias presentes na obra ou em parte dela; geralmente feito com as próprias palavras do estudante que ficha (faz o fichamento), segundo a sua interpretação pessoal da obra, ou parte dela. E por fim, o fichamento por citação, também conhecido por transcrição textual, é o fichamento no qual o estudante reproduz fielmente as frases do autor, isto é, cópia do jeito que está escrito no livro; com a indicação de página de onde foi retirada a parte transcrita ou copiada.

Isso é o fichamento, e falando nisso... percebeu que fizemos um exemplo de fichamento logo no/do primeiro parágrafo? Para você não voltar lá, vou transcrever aqui a frase apenas com as palavras selecionadas: “(...) fichamento é (...) o ato (...) de (...) reescrever informações selecionadas, (...), em fichas, para (...) organização (...) para usos posteriores.”

Reparou que a frase ainda faz sentido após o processo do fichamento? Essa é a ideia! E uma dica para facilitar a digitação do símbolo de supressão de texto, expresso pelas reticências entre parênteses (...), podemos usar a função autotexto presente na maioria dos editores de textos; aqui usei o LibreOffice Writer e minha referência nesse tópico é o canal da [Vivian Facundes](https://www.youtube.com/channel/UC0A_MbkDhIDv-eHr1mjGtnQ "Vivian Facundes"){target="_blank"}. Como últimos exemplos, abaixo temos uma animação (imagem gif) do processo de fichamento de um parágrafo e o link com o fichamento de um texto completo sobre análise de discurso; ambos textos acadêmicos.

![Exemplo de fichamento](fichamentoExemplo800x450.gif "Exemplo de fichamento")

[Mesmo exemplo anterior um pouco maior, abre em outra aba](exemploFichamentoParagrafo.gif "Exemplo de fichamento"){target="_blank"}

[Fichamento do texto Análise de discurso de Orlandi](analiseDiscursoFichamento.pdf "Fichamento do texto Análise de discurso de Orlandi"){target="_blank"}

Agora que você teve contato com o conceito de fichamento e o seu algoritmo, ou seja, a receita para se fazer um, só precisa praticar. Você pode começar com este texto, que foi escrito de forma verbosa justamente para servir de modelo para o exercício do fichamento. Bons estudos!

[Voltar para a página Textos...](../textos.html)