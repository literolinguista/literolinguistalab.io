## Lista de (desejos) presentes

> "A vida não pode ser economizada para amanhã. Acontece sempre no presente." Rubem Alves.

### Sobre a vida...

Domingo (25 de fevereiro) completo 45 anos. Diferente dos anos anteriores, não vou refletir sobre [crises existências](passagens.html){target="_blank"}. Vou compartilhar minha lista de (desejos) presentes (no final). A vida passa rápido e, se vocês querem me dar alegria (na forma de presentes), a hora é agora. 

Depois de certa idade, a gente percebe que quando perguntam "quantos anos você tem?", na verdade, na verdade, mesmo, essa pergunta é sobre uma estimativa do tempo que ainda nos resta. Claro que tenho quase 45 anos (terei), mas esses são os que já passaram, provavelmente não terei mais 45 anos futuros... Por isso gosto da frase: "A vida não perde tempo!"

E como todo narrador, menti mais uma vez (nunca confie no narrador). Mas apenas parcialmente, porque não me aprofundei sobre crises existenciais, só joguei uma parte para vocês (risos), seguraram?

Para me redimir, lembro que a vida é boa e quando uso a palavra “vida”, não quero que confundam com a palavra “mundo”. O mundo é esse monstro que lidamos todos os dias de nossas vidas. O mundo tem guerras, intolerâncias de todos os tipos, preconceitos mil, desesperança... mas a vida não. A vida é excelente e eu sei que você sente a diferença.

Quando você fecha seus olhos e respira lenta e profundamente, você sente a vida. O ar entrando nos seus pulmões e saindo vagarosamente, isso é a vida. Talvez por isso, respirar alivia nossa ansiedade, uma forma de desconectar do mundo e se reconectar com a vida. 

Da mesma forma, quando você está "morrendo" de sede e bebe aquela água refrescante, isso é a vida. Curioso que água boa nem tem gosto, nem cor, nem cheiro e mesmo assim, quando estamos sedentos, ela é a melhor coisa que podemos sentir, isso é a vida. Não quero me estender, sei que você sente, a vida basta por si só, não tem a ver com ter coisas, antes, como experienciá-las. 

> Sentir é saber com o coração.

Nos restar tentar viver da melhor maneira possível, com a graça de nossa senhora da bicicletinha (equilíbrio). Se der para fazer do mundo, principalmente ao nosso redor, um lugarzinho melhor, melhor ainda, porque a vida é excelente, é esperança e não perde tempo; devemos aproveitá-la! Então, reescrevendo a frase do Rubem Alves, "A vida não pode ser economizada para amanhã. Acontece sempre no presente. (Por que a vida é o presente!)".

***

Sobre os presentes (no sentido de ter), na verdade não preciso de nenhum item da lista abaixo, mas posso aproveitar bem qualquer um deles. A maioria, livros, que sempre repasso após ler, literalmente ou a partir de minhas aulas e escrita. Ah, em breve publico uma coleção de contos na Amazon, aviso vocês a data.

Um que gostaria muito é [“Irmãs da revolução: Antologia de ficção especulativa feminista”](https://www.amazon.com.br/dp/8576575515/?coliid=I21UTE9GJ3D6VH&colid=2FXBEU6GRW78K&psc=1&ref_=list_c_wl_lv_vv_lig_dp_it){target="_blank"}, mas aceito qualquer outro impresso de minhas pessoas autoras favoritas:

- [Aline Valek](https://www.alinevalek.com.br/blog/livros "Aline Valek"){target="_blank"}
- [Bárbara Carine](https://www.amazon.com.br/Livros-Barbara-Carine-Soares-Pinheiro/s?rh=n%3A6740748011%2Cp_27%3ABarbara+Carine+Soares+Pinheiro "Bárbara Carine"){target="_blank"}
- [Carla Akotirene Santos](https://www.amazon.com.br/stores/Carla-Akotirene/author/B07J44KM43?ref=ap_rdr&isDramIntegrated=true&shoppingPortalEnabled=true "Carla Akotirene Santos"){target="_blank"}
- [Conceição Evaristo](https://www.amazon.com.br/stores/Concei%C3%A7%C3%A3o-Evaristo/author/B001JOM2KC?ref=ap_rdr&isDramIntegrated=true&shoppingPortalEnabled=true "Conceição Evaristo"){target="_blank"}
- [Eliane Brum](https://www.amazon.com.br/stores/Eliane-Brum/author/B00JBERFTU?ref=ap_rdr&isDramIntegrated=true&shoppingPortalEnabled=true "Eliane Brum"){target="_blank"}
- [Jana Bianchi](https://janabianchi.com.br/portfolio-escrita "Jana Bianchi"){target="_blank"}
- [Jana Viscardi](https://www.amazon.com.br/s?i=stripbooks&rh=p_27%3AJana+Viscardi&s=relevancerank&text=Jana+Viscardi&ref=dp_byline_sr_book_1 "Jana Viscardi"){target="_blank"}
- [Maria Ferreira](https://www.amazon.com.br/stores/Maria-Ferreira/author/B0BQ5TXFDV?ref=ap_rdr&isDramIntegrated=true&shoppingPortalEnabled=true "Maria Ferreira"){target="_blank"}
- [oli maia](https://oliviamaia.net/livros "oli maia"){target="_blank"}
- [Sybylla, Momentum Saga](https://www.momentumsaga.com/p/ebooks-etc.html "Sybylla, Momentum Saga"){target="_blank"}

Vales-presentes dos oráculos (Amazon e Google) também valem para assinaturas de aplicativos, cursos (de escrita são sempre bem vindos) e mais livros, claro.

[Lista de desejos da Amazon](https://www.amazon.com.br/hz/wishlist/ls/2FXBEU6GRW78K?ref_=wl_share){target="_blank"}

É isso, 45 anos...

[Voltar para a página Textos...](../textos.html)