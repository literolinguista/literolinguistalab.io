## Sobre poesia...

> "A poesia é ao mesmo tempo um esconderijo e um alto-falante." Nadine Gordimer.

... um dia, meu sobrinho estava concentradíssimo na brincadeira dele e quando comecei a dedilhar uma canção no violão, notei que ele parou imediatamente e, depois de um tempinho, continuou brincando; mas de um jeito diferente. Quando terminei, ele deu um suspiro de alguns segundos e falou: "Que música bonita, tio!" Fiquei surpreso e, nesse momento, compreendi o que havia mudado nele: o foco da atenção. Naquele dia, ele tinha acabado de ser tocado pela poesia...

A poesia é essa abstração de beleza divina que nos toca o espírito. Às vezes, esse toque é pelo texto (poema), outras, pela voz (música), também pelas cores (artes plásticas), pelo corpo em movimento (dança e teatro)... Não raramente, ela se manifesta numa profusão de modalidades, como esta coleção musicada de haicais (gênero poético sintético de origem japonesa), que se germinou na mente de um [escritor](https://www.instagram.com/literolinguista), despontou botões no arranjo de um [músico](https://sergiodeslandes.blogspot.com), floresceu nas vozes de cantores e agora dispersa sementes em ouvidos atentos, para talvez, germinarem outros ciclos...


### Haicais

### 01

Uma gota reverbera a tempestade 

Folhas submergem sobre si

O rio segue imóvel

### 02

Pedras redondas

Riacho secando

Gotas de aviso

### 03

Na dança do vento

As nuvens deslizam

No abismo do céu

### 04

Mormaço de dúvidas

Súbita precipitação

Gotas de paz

### 05

A tarde se esvai

A noite se insinua

Os dias se acumulam

### 06

Na superfície limpa

Voam farelos de pão

Fragmentos a ponderar

### 07

Na cadeira vazia

A escuridão sussurra

A inevitável jornada

### 08

A criança tem dúvidas

O jovem respostas

O velho um espelho.

[Voltar para a página Textos...](../textos.html)
