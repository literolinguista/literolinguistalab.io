## Início

**Literolinguista:** s.n. i[e]migrante das fronteiras ilu[irri]sórias da linguística e litera[ora]tura.

> "... nem sempre o vazio é sinônimo de silêncio [(...)](# "às vezes, é pleno de significados")."

Oi, escrevo este site [(...)](# "em HTML e CSS puros"), apelidado de "sitezinho", para tentar organizar e publicar conteúdos relacionados a estudos pessoais, sob a licença a [Licença Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR "Licença Creative Commons"){target="_blank"}. As leituras que me ajudaram estão na página de [referências](referencias.html "Referências"). A fim de resumir texto, antigamente omitia informações [(...)](# "irrelevantes") entre parênteses (mas abondenei a prática em favor da acessibilidade); caso queira ler as que restaram, basta passar o mouse sobre eles.

Apesar de estático, este site está em constante revisão. Aqui não coletamos dados, não forçamos anúncios, manipulação algorítmica, enfim, nada de ruim que se popularizou pela Internet. O único código em JavaScript é referente ao tradutor VLibras. Se quiser fazer um site parecido ou entender como este foi feito, basta replicar e adaptar [(...)](# "e provavelmente melhorar") os scripts [(...)](# "de conversão (do formato .md para .html") que estão neste repositório no [Gitlab](https://gitlab.com/literolinguista/literolinguista.gitlab.io){target="_blank"}.

Boa leitura.

### Últimos textos...

- [ Sobre a escrita...](textos/escrita.html "Sobre a escrita...") (12-11-2024 - escrita, reflexão)
- [Sobre o dia dos professores...](textos/professores.html "Sobre o dia dos professores...") (15-10-2024 - educação, pedido)
- [Sobre Contos revistos (autopublicação na Amazon)...](textos/contos-revistos.html "Sobre Contos revistos (autopublicação na Amazon)...") (12-10-2024 - livro, amazon)
- [Sobre poesia (haikais)...](textos/poesia.html "Sobre poesia (haikais)...") (25-09-2024 - música, haikais)
- [Sobre o pensamento computacional e o poético...](textos/pensamento-computacional.html "Sobre o pensamento computacionao e poétco...") (06.07.2024 - reflexão)
- [Sobre 'Café e romance' (livro da Denise Gals)...](textos/cafe-romance.html "Sobre 'Café e romance' (livro da Denise Gals)...") (12.05.2024 - indicação de leitura)
- [Sobre 'capaternismo' ou 'patercitismo'...](textos/capacitismo.html "Sobre 'capaternismo' ou 'patercitismo'...") (28.04.2024 - reflexão)
- [Sobre o dia das mulheres...](textos/mulheres.html "Sobre o dia das mulheres...") (08.03.2024 - reflexão)
- [Lista de (desejos) presentes...](textos/vida-presente.html "Lista de (desejos) presentes...") (22.02.2024 - aniversário)
- [Sites para baixar livros grátis...](textos/livros-sites.html "Sites para baixar livros grátis...") (18.02.2024 - indicação)
- [Todos os textos do Recanto das letras...](textos/recantoLetras/index.html "Todos os textos do Recanto das letras..."){target="_blank"} (16.01.2024 - coleção)
- [Sobre a pedagogia do encanto...](textos/encanto.html "Sobre a pedagogia do encanto...") (18.05.2023 - reflexão)
- [Sobre a PL2639...](textos/pl2630.html "Sobre a PL2630...") (02.05.2023 - letramento digital)
- [Sobre hacker...](textos/hacker.html "Sobre hacker...") (27.04.2023 - letramento digital)
- [Informática: conceitos básicos...](textos/informatica-conceitos-basicos.html "Informática: conceitos básicos...") (04.03.2023 - letramento digital)
- [Sobre fichamento...](textos/fichamento.html "Sobre fichamento...") (03.03.2023 - técnica de estudo)
- [Sobre senhas...](textos/senhas.html "Sobre senhas...") (03.03.2023 - letramento digital)
- [Sobre passagens...](textos/passagens.html "Sobre passagens...") (24.02.2023 - pensamento)
- [Sobre séries marcantes...](textos/series.html "Sobre séries marcantes...") (22.02.2023 - indicação)
- [Sobre o tempo...](textos/tempo.html "Sobre o tempo...") (20.02.2023 - reflexão)
- [Sobre carta para a pessoa amiga...](textos/carta.html "Sobre carta para a pessoa amiga...") (05.02.2023 - reflexão)
- [Sobre o mito da doutrinação escolar...](textos/mito-doutrincao.html "Sobre o mito da doutrinação escolar...") (13.09.2022 - educação)
- [À propos du régime végétarien...](textos/vegetarien.html "À propos du régime végétarien...")  (20.10.2021 - réflexion)
- [À propos de Marceline Desbordes-Valmore...](textos/marceline.html "À propos de Marceline Desbordes-Valmore...") (20.10.2021 - littérature)

Atualizado numa tarde escaldante de terça-feira (12.11.2024).
