## Textos

> “Escrever é fácil. Você começa com uma letra maiúscula e termina com um ponto final. No meio você coloca idéias.” Pablo Neruda.

### Textos inacabados sobre assuntos diversos, sem ordem cronológica ou bases confiáveis ou mesmo nexo...

- [ Sobre a escrita...](textos/escrita.html "Sobre a escrita...") (12-11-2024 - escrita, reflexão)
- [Sobre o dia dos professores...](textos/professores.html "Sobre o dia dos professores...") (15-10-2024 - educação, pedido)
- [Sobre Contos revistos (autopublicação na Amazon)...](textos/contos-revistos.html "Sobre Contos revistos (autopublicação na Amazon)...") (12-10-2024 - livro, amazon)
- [Sobre poesia (haikais)...](textos/poesia.html "Sobre poesia (haikais)...") (25-09-2024 - música, haikais)
- [Sobre o pensamento computacional e o poético...](textos/pensamento-computacional.html "Sobre o pensamento computacionao e poétco...") (06.07.2024 - reflexão) 
- [Sobre 'Café e romance' (livro da Denise Gals)...](textos/cafe-romance.html "Sobre 'Café e romance' (livro da Denise Gals)...") (12.05.2024 - indicação de leitura)
- [Sobre 'capaternismo' ou 'patercitismo'...](textos/capacitismo.html "Sobre 'capaternismo' ou 'patercitismo'...") (28.04.2024 - reflexão)
- [Sobre o dia das mulheres...](textos/mulheres.html "Sobre o dia das mulheres...") (08.03.2024 - reflexão)
- [Lista de (desejos) presentes...](textos/vida-presente.html "Lista de (desejos) presentes...") (22.02.2024 - aniversário)
- [Sites para baixar livros grátis...](textos/livros-sites.html "Sites para baixar livros grátis...") (18.02.2024 - indicação)
- [Todos os textos do Recanto das letras...](textos/recantoLetras/index.html "Todos os textos do Recanto das letras..."){target="_blank"} (16.01.2024 - coleção)
- [Sobre a pedagogia do encanto...](textos/encanto.html "Sobre a pedagogia do encanto...") (18.05.2023 - reflexão)
- [Sobre a PL2639...](textos/pl2630.html "Sobre a PL2630...") (02.05.2023 - letramento digital)
- [Sobre hacker...](textos/hacker.html "Sobre hacker...") (27.04.2023 - letramento digital)
- [Informática: conceitos básicos...](textos/informatica-conceitos-basicos.html "Informática: conceitos básicos...") (04.03.2023 - letramento digital)
- [Sobre fichamento...](textos/fichamento.html "Sobre fichamento...") (03.03.2023 - técnica de estudo)
- [Sobre senhas...](textos/senhas.html "Sobre senhas...") (03.03.2023 - letramento digital)
- [Sobre passagens...](textos/passagens.html "Sobre passagens...") (24.02.2023 - pensamento)
- [Sobre séries marcantes...](textos/series.html "Sobre séries marcantes...") (22.02.2023 - indicação)
- [Sobre o tempo...](textos/tempo.html "Sobre o tempo...") (20.02.2023 - reflexão)
- [Sobre carta para a pessoa amiga...](textos/carta.html "Sobre carta para a pessoa amiga...") (05.02.2023 - reflexão)
- [Sobre o mito da doutrinação escolar...](textos/mito-doutrincao.html "Sobre o mito da doutrinação escolar...") (13.09.2022 - educação)
- [À propos du régime végétarien...](textos/vegetarien.html "À propos du régime végétarien...")  (20.10.2021 - réflexion)
- [À propos de Marceline Desbordes-Valmore...](textos/marceline.html "À propos de Marceline Desbordes-Valmore...") (20.10.2021 - littérature)

[Voltar ao início...](index.html)
