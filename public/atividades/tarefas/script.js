let currentTasks = [];
let currentIndex = 0;
const indices = 'abcdefghijklmnopqrstuvwxyz';

function showNewListForm() {
    document.getElementById('taskForm').classList.add('active');
    document.getElementById('savedLists').classList.remove('active');
    currentTasks = [];
    currentIndex = 0;
    clearMatrix();
}

function showSavedLists() {
    document.getElementById('savedLists').classList.add('active');
    document.getElementById('taskForm').classList.remove('active');
    loadSavedLists();
}

function removeTask(index) {
    currentTasks = currentTasks.filter(task => task.indice !== index);
    updateMatrix();
}

function addTask() {
    if (currentIndex >= 26) {
        alert('Limite máximo de tarefas (26) atingido!');
        return;
    }

    const taskName = document.getElementById('taskName').value;
    const priority = parseInt(document.getElementById('priority').value);

    if (!taskName || isNaN(priority) || priority < 1 || priority > 4) {
        alert('Por favor, preencha todos os campos corretamente. A prioridade deve ser entre 1 (maior) e 4 (menor).');
        return;
    }

    const task = {
        indice: indices[currentIndex],
        tarefa: taskName,
        prioridade: priority
    };

    currentTasks.push(task);
    currentIndex++;

    document.getElementById('taskName').value = '';
    document.getElementById('priority').value = '';

    updateMatrix();
}

function finishList() {
    if (currentTasks.length === 0) {
        alert('Adicione pelo menos uma tarefa à lista.');
        return;
    }

    const listName = document.getElementById('listName').value;
    if (!listName) {
        alert('Por favor, digite um nome para a lista.');
        return;
    }

    const date = new Date().toLocaleDateString('pt-BR').split('/').join('-');
    const fileName = `${listName}_${date}`;

    const savedLists = JSON.parse(localStorage.getItem('taskLists') || '{}');
    savedLists[fileName] = currentTasks;
    localStorage.setItem('taskLists', JSON.stringify(savedLists));

    alert('Lista salva com sucesso!');
    document.getElementById('taskForm').classList.remove('active');
    document.getElementById('listName').value = '';
}

function loadSavedLists() {
    const savedLists = JSON.parse(localStorage.getItem('taskLists') || '{}');
    const container = document.getElementById('savedListsContainer');
    container.innerHTML = '';

    Object.keys(savedLists).forEach(listName => {
        const li = document.createElement('li');
        li.textContent = listName;
        li.onclick = () => loadList(listName);
        container.appendChild(li);
    });
}

function loadList(listName) {
    const savedLists = JSON.parse(localStorage.getItem('taskLists') || '{}');
    currentTasks = savedLists[listName];
    updateMatrix();
    document.getElementById('savedLists').classList.remove('active');
}


function updateMatrix() {
    clearMatrix();

    currentTasks.forEach(task => {
        const taskElement = document.createElement('li');
        let actionDescription = '';

        switch (task.prioridade) {
            case 1:
                actionDescription = 'fazer imediatamente!';
                document.getElementById('ui-tasks').appendChild(taskElement);
                break;
            case 2:
                actionDescription = 'fazer assim que puder...';
                document.getElementById('nui-tasks').appendChild(taskElement);
                break;
            case 3:
                actionDescription = 'fazer o mais breve possível...';
                document.getElementById('uni-tasks').appendChild(taskElement);
                break;
            case 4:
                actionDescription = 'fazer em agosto (a gosto de Deus...)...';
                document.getElementById('nuni-tasks').appendChild(taskElement);
                break;
        }

        taskElement.innerHTML = `
                    <span>${task.indice}) ${task.tarefa} - ${actionDescription}</span>
                    <button class="remove-btn" onclick="removeTask('${task.indice}')">Remover</button>
                `;
    });
}

function clearMatrix() {
    document.getElementById('ui-tasks').innerHTML = '';
    document.getElementById('uni-tasks').innerHTML = '';
    document.getElementById('nui-tasks').innerHTML = '';
    document.getElementById('nuni-tasks').innerHTML = '';
}