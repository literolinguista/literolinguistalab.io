## Sobre

> “Não sou contra nem a favor, muito pelo contrário...” Ditado popular.

### Micro bio...

Jackson é sotero-recifense [(...)](# "nascido em Salvador com um pé em Recife"), tenta atuar na área de [educação](http://lattes.cnpq.br/8755541190873634 "Currículo Lattes"){target="_blank"}, sempre fica na dúvida se deixa de usar [(...)](# "e ser usado por") “redes sociais” e se interessa por tecnologia, linguagens e suas diversas modalidades.

Gosta do [FreeBSD](https://www.freebsd.org "FreeBSD"){target="_blank"}, mas preza pela liberdade de software, sendo um feliz utilizador do [Debian GNU/Linux](https://www.debian.org "Debian GNU/Linux"){target="_blank"}. Perdido pelo terminal, alterna entre o [i3-wm](https://i3wm.org "i3-wm"){target="_blank"} e o [xmonad](https://xmonad.org "xmonad"){target="_blank"}.

É baixo natural [(...)](# "mas queria mesmo era ser barítono"), fez parte do extinto coro [Ars Canticus](https://www.instagram.com/coralarscanticus "Ars Canticus"){target="_blank"} e neste [canal no Odysse](https://odysee.com/@jacksondejesus:1 "canal pessoal no Odysse"){target="_blank"} esconde [(...)](# "as desafinações e nervosismos de") algumas apresentações.  Por fim, é metido a dublê de escritor no antigo blog [Meiotexto](https://meiotexto.wordpress.com "Meiotexto"){target="_blank"} e no [Recanto das letras](https://www.recantodasletras.com.br/autores/literolinguista "Recanto das letras"){target="_blank"}.

[Voltar ao início...](index.html)